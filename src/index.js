const AWS = require('aws-sdk')


function getConfig() {
    if (process.env.NODE_ENV == 'test' || process.env.IS_OFFLINE || process.env.IS_CLI) {
        if (process.env.CI) {
            return {
                region: 'localhost',
                endpoint: 'http://dynamodb:8000',
                accessKeyId: 'DEFAULT_ACCESS_KEY',  // needed if you don't have aws credentials at all in env
                secretAccessKey: 'DEFAULT_SECRET',
            } // needed if you don't have aws credentials at all in env 
        } else {
            return {
                region: 'localhost',
                endpoint: 'http://localhost:8000',
                accessKeyId: 'DEFAULT_ACCESS_KEY',  // needed if you don't have aws credentials at all in env
                secretAccessKey: 'DEFAULT_SECRET'
            }
        }
    }
    return {
        region: AWS.config.region,
        endpoint: AWS.config.endpoint,
        accessKeyId: AWS.config.accessKeyId,
        secretAccessKey: AWS.config.secretAccessKey 
    }
}

function setup() {
    Object.assign(AWS.config, getConfig())
}

module.exports = { setup, getConfig }